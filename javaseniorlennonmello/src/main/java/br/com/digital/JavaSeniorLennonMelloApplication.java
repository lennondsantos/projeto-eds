package br.com.digital;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaSeniorLennonMelloApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaSeniorLennonMelloApplication.class, args);
	}
	
}
