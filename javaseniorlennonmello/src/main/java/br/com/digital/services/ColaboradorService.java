package br.com.digital.services;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import br.com.digital.model.Colaborador;
import br.com.digital.repository.ColaboradorRepository;
import br.com.digital.services.exception.ObjetoNaoEncontradoException;

@Service
public class ColaboradorService {
	
	protected ColaboradorRepository colaboradorRepository;
	
	 public ColaboradorService(ColaboradorRepository colaboradorRepository) {
		 this.colaboradorRepository = colaboradorRepository;
	}

	public List<Colaborador> buscarTodos(){
		List<Colaborador> colabodores = colaboradorRepository.findAll();        
        return colabodores;

	}
	
	public Colaborador buscarPor(Integer id) {
		Optional<Colaborador> obj = colaboradorRepository.findById(id);
		if(!obj.isPresent()) {
			throw new ObjetoNaoEncontradoException("O colaborador que você está procurando não existe");
		}
		
		return obj.get();
	}
	
	public void atualizar(Colaborador colaborador) {
		colaboradorRepository.save(colaborador);
	}
	
	public void remover(Integer id) {
		colaboradorRepository.delete(this.buscarPor(id));		
	}
	
	public Colaborador cadastrar(Colaborador colaborador) {		
		return colaboradorRepository.save(colaborador);
	}
	
	public Colaborador buscarPorCpf(String cpf) {
		Colaborador colaborador = colaboradorRepository.findByCpf(cpf);
		return colaborador;
	}

}
