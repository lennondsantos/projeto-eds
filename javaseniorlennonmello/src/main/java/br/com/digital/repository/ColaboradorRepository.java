package br.com.digital.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.digital.model.Colaborador;

public interface ColaboradorRepository extends JpaRepository<Colaborador, Integer>{
	
	Colaborador findByCpf(String cpf);

}
