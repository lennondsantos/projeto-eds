package br.com.digital.resources.exception;

public class ErroPadrao {
	
	private Integer status;
	private String msg;
	
	public ErroPadrao(String msg, int status) {
		this.msg = msg;
		this.status = status;
	}
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}

	

}
