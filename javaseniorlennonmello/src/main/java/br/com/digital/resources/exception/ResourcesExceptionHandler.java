package br.com.digital.resources.exception;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import br.com.digital.services.exception.ObjetoNaoEncontradoException;

@ControllerAdvice
public class ResourcesExceptionHandler {

	@ExceptionHandler(ConstraintViolationException.class)
	public ResponseEntity<ErroPadrao> constraintValidation(ConstraintViolationException e,HttpServletRequest request){
		ErroPadrao erro = new ErroPadrao("Violacao de chave estrangeira",HttpStatus.BAD_REQUEST.value());
		
		return ResponseEntity.status(erro.getStatus()).body(erro);
	}

	@ExceptionHandler(ObjetoNaoEncontradoException.class)
	public ResponseEntity<ErroPadrao> handleObjetoNaoEncontrado(ObjetoNaoEncontradoException e,HttpServletRequest request){
		ErroPadrao erro = new ErroPadrao(e.getMessage(), HttpStatus.NOT_FOUND.value());
		
		return ResponseEntity.status(erro.getStatus()).body(erro);
	}
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<ErroPadrao> handleObjetoNaoEncontrado(MethodArgumentNotValidException e,HttpServletRequest request){
		ErroPadrao erro = new ErroPadrao("Algum campo esta nullo...", HttpStatus.BAD_REQUEST.value());
		
		return ResponseEntity.status(erro.getStatus()).body(erro);
	}
	
}
