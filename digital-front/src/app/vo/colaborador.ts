import { Setor } from './setor';

export class Colaborador{
  public id: number;
	public nome: string;
	public cpf: number;
  public email: string;
  public setor: Setor;
  public telefone: string;
  constructor(){}
}
