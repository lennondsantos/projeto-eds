import { Component, OnInit } from '@angular/core';
import { ColaboradorService } from '../../services/colaborador-service/colaborador-service';
import { Colaborador } from '../../vo/colaborador';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-colaborador-listar',
  templateUrl: './colaborador-listar.component.html',
  styleUrls: ['./colaborador-listar.component.css']
})
export class ColaboradorListarComponent implements OnInit {

  colaboradores:Colaborador[] = [];

  constructor(private colaboradorService:ColaboradorService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.consultarTodos();
  }

  novo(){
    this.router.navigate([`criar`], { relativeTo: this.route });
  }

  editar(id:number){
    this.router.navigate([`criar/${id}`], { relativeTo: this.route });
  }

  excluir(id:number){
    this.colaboradorService.excluir(id).subscribe(() =>{
      this.consultarTodos();
    })
  }

  consultarTodos(){
    this.colaboradorService.consultarTodos().subscribe((cols:Colaborador[]) => this.colaboradores = cols)
  }

}
