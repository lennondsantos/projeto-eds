import { Component, OnInit } from '@angular/core';
import { Colaborador } from '../../vo/colaborador';
import { ColaboradorService } from '../../services/colaborador-service/colaborador-service';
import { Router, ActivatedRoute } from '@angular/router';
import { ErroPadrao } from '../../vo/erro-padrao';

@Component({
  selector: 'app-colaborador-criar',
  templateUrl: './colaborador-criar.component.html',
  styleUrls: ['./colaborador-criar.component.css']
})
export class ColaboradorCriarComponent implements OnInit {

  colaborador:Colaborador = new Colaborador();

  constructor(private colaboradorService:ColaboradorService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
    const id: number = parseInt(this.route.snapshot.params['id']);
    if(id){
      this.colaboradorService.consultar(id).subscribe((col:Colaborador) => {
        this.colaborador = col
      });
    }else{
      this.colaborador = new Colaborador();
    }

  }

  criar(){
    this.colaboradorService.criar(this.colaborador).subscribe(() =>{
      this.routerVoltar();
    },(err:ErroPadrao) => console.log('Status: '+err.status+' msg: '+err.msg))
  }

  alterar(){
    this.colaboradorService.alterar(this.colaborador).subscribe(() =>{
      this.routerVoltar();
    },(err:ErroPadrao) => console.log('Status: '+err.status+' msg: '+err.msg))
  }

  voltar(){
    this.routerVoltar();
  }

  routerVoltar(){
    this.router.navigate(['/colaborador'], { relativeTo: this.route.parent });
  }

}
