import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ColaboradorCriarComponent } from './colaborador-criar.component';

describe('ColaboradorCriarComponent', () => {
  let component: ColaboradorCriarComponent;
  let fixture: ComponentFixture<ColaboradorCriarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ColaboradorCriarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColaboradorCriarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
