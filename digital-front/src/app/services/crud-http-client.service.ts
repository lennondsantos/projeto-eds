import { HttpClient } from '@angular/common/http';


import { HttpClientService } from './http-client.service';
import { Observable } from 'rxjs';

export class CrudHttpClientService<T> extends HttpClientService {
	constructor(public uri: string, protected http: HttpClient) {
		super(uri, http);
	}

	/**
	 * Recupera um registro em particular, ou todos (caso não seja passado
	 * o parâmetro id)
	 * @param id
	 */
	public get(id?: number): Observable<T> {
		let url = this.url;

		if (id) {
				url += '/' + id;
		}

		return this.http.get<T>(url, this.options());
	}

	/**
	 * Insere um registro
	 * @param entity
	 */
	public post(entity: T): Observable<T> {
		return this.http.post<T>(this.url, entity, this.options());
	}

	/**
	 * Altera um registro
	 * @param entity
	 */
	public put(entity: T): Observable<T> {
		return this.http.put<T>(this.url, entity, this.options());
	}

	/**
	 * Exclui um registro
	 * @param id
	 */
	public delete(id: number): Observable<T> {
		return this.http.delete<T>(this.url + '/' + id, this.options());
	}




}
