import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Colaborador } from '../../vo/colaborador';
import { CrudHttpClientService } from '../crud-http-client.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ColaboradorService extends CrudHttpClientService<Colaborador> {

	constructor(protected http: HttpClient) {
		super('colaboradores', http);
	}

	/**
	 * Consulta e recebe um erro customizado
	 */
	public consultarTodos(): Observable<Colaborador[]> {
		return this.http.get<Colaborador[]>(this.url);
  }

  public criar(col:Colaborador) {
		return this.http.post<Colaborador>(this.url,col);
  }

  public consultar(id:number): Observable<Colaborador> {
		return this.http.get<Colaborador>(this.url+'/'+id);
  }

  public alterar(col:Colaborador) {
		return this.http.put<Colaborador>(this.url+'/'+col.id,col);
  }

  public excluir(id:number) {
		return this.http.delete<Colaborador>(this.url+'/'+id);
  }
}
