import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ColaboradorComponent } from './colaborador/colaborador.component';
import { ColaboradorListarComponent } from './colaborador/colaborador-listar/colaborador-listar.component';
import { ColaboradorCriarComponent } from './colaborador/colaborador-criar/colaborador-criar.component';


const routes: Routes = [
  {
    path: 'colaborador',
    component: ColaboradorComponent,
    children: [
      { path: '', component: ColaboradorListarComponent},
      { path: 'criar', component: ColaboradorCriarComponent },
      { path: 'criar/:id', component: ColaboradorCriarComponent }
    ]
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
